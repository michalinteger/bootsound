#!/usr/bin/env bash
PERSDATADIR="/var/lib/bootsound/"
CONFIG_FILE="$PERSDATADIR/config"
if [[ -f $CONFIG_FILE ]]; then
    source "$CONFIG_FILE"
    SOUNDFILE="$PERSDATADIR$SOUNDFILENAME"
    if [ $VOLUME ]; then
        echo "Setting volume of $AMIXER_NAME to $VOLUME (card $AMIXER_CARDNUM)"
        /usr/bin/amixer -q -c $AMIXER_CARDNUM sset $AMIXER_NAME "$VOLUME"
    fi
    echo "Playing $SOUNDFILE to $DEVICE"
    /usr/bin/aplay -D $DEVICE "$SOUNDFILE"
else
    echo "No config file found."
    exit 1
fi
