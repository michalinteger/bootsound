#!/usr/bin/env bash
rm  /usr/bin/bootsound-play \
    /usr/lib/systemd/system/bootsound.service \
    /usr/share/licenses/bootsound-git/LICENSE \
    /var/lib/bootsound/config.default
