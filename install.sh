#!/usr/bin/env bash
target=$1
install -Dm644 "LICENSE" "$target/usr/share/licenses/bootsound-git/LICENSE"
install -Dm644 "bootsound.service" "$target/usr/lib/systemd/system/bootsound.service"
install -Dm755 "bootsound.sh" "$target/usr/bin/bootsound-play"
install -Dm644 "config.default" "$target/var/lib/bootsound/config.default"
