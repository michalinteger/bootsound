# Bootsound

Systemd service that plays a sound at boot with alsa.

## Installation

1. Clone this repo
1. Run `bash install.sh` as root
1. Copy a wave file to `/var/lib/bootsound/<soundname>.wav`
1. Copy `/var/lib/bootsound/config.default` to `/var/lib/bootsound/config` and set `SOUNDFILENAME` to the name of the wave file
1. Run `systemctl enable bootsound.service` as root

## Uninstallation

1. Run `bash uninstall.sh` as root
1. OPTIONAL: If you want to remove sounds and configuration run `rm -r /var/lib/bootsound`
